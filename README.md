# ioChem-BD in Docker containers 

[ioChem-BD platform](https://www.iochem-bd.org) is a series of Java web services for managing computational chemistry results.
It is coded to run on a UNIX/Linux server but, in order to overcome this limitation, a docker image has been created to run it on [multiple platforms](https://docs.docker.com/install/#supported-platforms).

Current project contains all the necessary files to easily deploy [ioChem-BD chemical software](https://www.iochem-bd.org) by the use of containers. It allows to run ioChem-BD with no dependencies or prerequisites except the [docker tool](https://www.docker.com/). The deployment is customizable by modifying a configuration file bundled inside this project.

## Requirements

Required packages:
  - [docker](https://docs.docker.com/install/) tool
  - [docker-compose](https://docs.docker.com/compose/install/) utility (optional, but advised to simplify deployment)

------

## TL;DR  *for the impatient*

Go from zero to ioChem-BD in a simple command line:

```shell
$ docker run -d --ulimit nofile=20000:65535 -m 8192m  --name iochem-bd-with-data --add-host test.iochem-bd.org:127.0.0.1 -p 8443:8443 --hostname test.iochem-bd.org iochembd/iochem-bd-docker:latest-with-data
```

Previous command will start a container of an ioChem-BD installation with some user accounts created and a few calculations uploaded to them, so you can play around it, the startup process can take a couple of minutes.

To make the container run properly, please reserve at least 6GB of memory for the container with the *-m 6144m*  parameter.

Must also add this entry to [your *hosts* file](#editing-hosts-file) to route properly to the running container:

```bash
127.0.0.1  test.iochem-bd.org
```

Now point the browser to *https://test.iochem-bd.org:8443* and after a complain about the self-signed certificate, it will show the ioChem-BD main page.

The administrator account has the following default credentials:
  * username: whatever@example.com
  * password = mypassword

With this user account you can start creating users, user groups and communities where to publish your content following the [ioChem-BD documentation](https://docs.iochem-bd.org/en/latest/guides/installation/user-and-group-generation.html#creating-users-and-groups).


------

## Project structure

It contains two folders:
 - */docker-compose* : For running ioChem-BD [using docker-compose](#run-it-using-docker-compose) utility.
 - */docker* : It contains the required *Dockerfile* and */resources* folder to build a new ioChem-BD docker image from scratch, just in case you want to add more packages or customizations to the vanilla image. 

The images are available on the following Docker registry services:
  - Docker Hub:  [iochembd/iochem-bd-docker](https://hub.docker.com/r/iochembd/iochem-bd-docker) repository

## How to start the container

### Mode 1) Run it using docker-compose

This is the most straightforward method to configure and run the container. First, download this project to a local folder, then run docker-compose: 

```bash
$   git clone https://gitlab.com/ioChem-BD/iochem-bd-docker.git
$   cd iochem-bd-docker/docker-compose
$   docker-compose up
```

It will download the latest image, set it up and run it with the default values:

| parameter | value  |
| --- | --- |
| hostname |  test.iochem-bd.org  |
| web service port | 8443 |
| admin credentials | defined on */custom/default.config* file |
| data contained  |  Without data. In case you want to use the image with demo data, edit *docker-compose.yml* and set **image** parameter to *iochembd/iochem-bd-docker:latest-with-data*  |

Must also add this entry to [your *hosts* file](#editing-hosts-file) to enroute properly to the running container:

```bash
127.0.0.1  test.iochem-bd.org
```

Now point the browser to *https://test.iochem-bd.org:8443* and after a complain about the self-signed certificate, it will show the ioChem-BD main page.

#### Additional actions

Docker-compose tool allow to run, stop and delete containers by reading the *docker-compose.yml* file. 

```bash
$   cd docker-compose           # Always move to the base *docker-compose* folder to run the commands

$   docker-compose logs -f      # View the system log of the container (check for error and other info)           
$   docker-compose stop         # Stop the container
$   docker-compose start        # Start the container
$   docker-compose rm -fv       # Remove the container (must be stopped first)
```

#### Custom deployment

All parameters used during the ioChem-BD setup are defined inside *docker-compose.yml* and *default-config* files. 

Move to */docker-compose* folder and review the following elements:
 - docker-compose.yml : The composer file used to setup and launch the container
 - /custom/default.config : Custom configurations applied to the container

### Mode 2) From command line

ioChem-BD image can be run from command line with no downloads at all. Just type in this command:

```bash
$    docker run -d --ulimit nofile=20000:65535 -m 8192m --name iochem-bd-docker --add-host test.iochem-bd.org:127.0.0.1 -p 8443:8443 --hostname test.iochem-bd.org iochem-bd/iochem-bd:latest

# Parameters used:
#   run = Start new container
#   -d  = Detach mode (run in the background)
#   --name  = Name of the container inside docker
#   -p = Port used inside docker is mapped to a port in the host machine (can't be changed in this run mode)
#   --hostname = hostname defined inside container (can't be changed in this run mode)
```

And add this entry to [your *hosts* file](#editing-hosts-file) to enroute properly to the running container:

```bash
127.0.0.1  test.iochem-bd.org
```

Now point you browser to *https://test.iochem-bd.org:8443* and after a complain about the self-signed certificate it will show the ioChem-BD main page. 

#### Additional actions

```bash
$   docker ps     # List running containers
$   docker logs -f iochem-bd-docker     # View the system log of the container (check for error and other info)
$   docker exec -ti iochem-bd-docker /bin/bash       # Login inside the container
$   docker stop iochem-bd-docker    # Stop the container
$   docker start iochem-bd-docker   # Start an stopped container
$   docker rm -fv iochem-bd-docker  # Remove the container (must be stopped first) 
```

#### Custom deployment

In order to configure a different server port, hostname or change the administrator account credentials, you must provide a configuration file and mount it as a volume of the container.

First download the */docker/resources/default.config* file from this project and drop it on a folder, let's name it *custom* 
Now edit the file and set a different value on **hostPort** property like 443.

Now we will run the container adding this folder as a volume mounted inside the container. The installation procedure will detect it and setup the system accordingly.
Launch the *docker run* varying the -p parameter and adding the volume:

```bash
$  docker run -d --ulimit nofile=20000:65535 -m 8192m --name iochem-bd-docker --add-host test.iochem-bd.org:127.0.0.1 -v ${PWD}/custom:/tmp/iochembd-resources -p 443:443 --hostname test.iochem-bd.org iochem-bd/iochem-bd:latest 
```


---

## Editing *hosts* file 

In order to allow communication with the new running container, the host running docker service must know the IP assigned to the new ioChem-BD container, in this case it is localhost (127.0.0.1). 
The file depends on the host OS:

| Operating System | Location |
| --- | --- |
| Unix, Unix-like, POSIX  | /etc/hosts |
| Microsoft Windows | %SystemRoot%\System32\drivers\etc\hosts |
| Apple Macintosh  | 	/etc/hosts (a symbolic link to /private/etc/hosts) |

### A note about security

If you run the vanilla ioChem-BD image (without changing any parameter) and intend to run it on production, please update administrator account password from the Browse module web interface or create a new one and delete the original.
