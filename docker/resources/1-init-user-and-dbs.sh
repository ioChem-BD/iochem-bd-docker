#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER iochembd WITH PASSWORD 'iochembd';
    CREATE DATABASE "iochemCreate" OWNER iochembd;
    CREATE DATABASE "iochemBrowse" OWNER iochembd;
    GRANT ALL PRIVILEGES ON DATABASE "iochemCreate" TO iochembd;
    GRANT ALL PRIVILEGES ON DATABASE "iochemBrowse" TO iochembd;
EOSQL
