#!/bin/bash

# SIGKILL-handler
kill_handler() {
  echo -en "\n## Caught SIGKILL; Kill processes and Exit \n"
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  # End log tail
  pkill --signal SIGKILL tail
  exit $?
}

# SIGTERM-handler
term_handler() {
    echo -en "\n## Caught SIGTERM; Clean up and Exit \n"
    # Stop Apache Tomcat
    /home/iochembd/iochembd/apache-tomcat/bin/shutdown.sh
    sleep 10
}

# Retrieve test certificates
update_certs() {
   instance_hostname=$(sed -n  -r 's/hostName\s*=\s*(.*)/\1/p' /tmp/iochembd-resources/default.config)
   if [ $instance_hostname == 'test.iochem-bd.org' ]; then  #Demo instance, get temporal certs
        sudo -u iochembd /bin/bash -c "cd /home/iochembd/iochembd/ssl/new; wget --no-check-certificate https://www.iochem-bd.org/update/certs/certificate.bundle"
        sudo -u iochembd /bin/bash -c "cd /home/iochembd/iochembd/ssl/new; wget --no-check-certificate https://www.iochem-bd.org/update/certs/certificate.crt"
        sudo -u iochembd /bin/bash -c "cd /home/iochembd/iochembd/ssl/new; wget --no-check-certificate https://www.iochem-bd.org/update/certs/certificate.key"
        if [ -f /home/iochembd/iochembd/ssl/new/certificate.crt ]; then
                if [ -f /home/iochembd/iochembd/ssl/new/certificate.key ]; then        # Got the proper certificates, update them
                        cd /home/iochembd/iochembd/updates
                        sudo -u iochembd /bin/bash -c "cd /home/iochembd/iochembd/updates; ./updater.sh -p ReplaceDomainCertificate"
                        rm -f updater.jar
                fi
        fi
   fi
}

# Add sample data: files and sql must be inserted automatically
add_sample_data() {
    if [ $IOCHEMBD_WITH_DATA == 'true'  ]; then
        sudo -u iochembd /bin/bash -c "cd /tmp; wget --no-check-certificate https://www.iochem-bd.org/update/docker/sample_data.tar.gz"
        sudo -u iochembd /bin/bash -c "cd /tmp; tar -xvf sample_data.tar.gz; cd /;tar -xvf /tmp/sample_data/data.tar.gz"
        sudo -u postgres /bin/bash -c "psql \"iochemCreate\" < /tmp/sample_data/sql/create.sql;"
        sudo -u postgres /bin/bash -c "psql \"iochemBrowse\" < /tmp/sample_data/sql/browse.sql;"
        rm -fr /tmp/sample_data
        rm -f /tmp/sample_data.tar.gz
    fi
}

/usr/local/bin/docker-entrypoint.sh postgres &

# Wait until PostgreSQL is enabled
echo "Please wait until the PostgreSQL server starts, it can take more than one minute"
sleep 10
pg_isready
while [ ! $? -eq 0 ]
do
    sleep 1
    pg_isready
done

# Check software is installed 
if [ -f /home/iochembd/iochembd/init-script/installed ]; then
    echo "ioChem-BD already installed, starting web service"
else
    echo "Initial startup, will start ioChem-BD installation process."
    sudo -i -u iochembd /home/iochembd/iochembd/init.sh -c /tmp/iochembd-resources/default.config
    . /etc/profile.d/iochembd-vars.sh
    /home/iochembd/iochembd/postinstall.sh
    add_sample_data
    touch /home/iochembd/iochembd/init-script/installed
    update_certs
fi

# Capture docker stop signals
trap 'kill ${!}; term_handler' SIGTERM
trap 'kill ${!}; kill_handler' SIGKILL

# Start web server
echo "Starting Apache Tomcat"
sudo -i -u iochembd /home/iochembd/iochembd/apache-tomcat/bin/startup.sh 
pid="$!"

# Endless loop with a tail, otherwise container will stop
while true
do
    tail -f /home/iochembd/iochembd/apache-tomcat/logs/catalina.out & wait ${!}
done
