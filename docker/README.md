# Building ioChem-BD images from the Dockerfile 

The current folder contains a Dockerfile with all the commands to set up a distributable [ioChem-BD](https://gitlab.com/ioChem-BD/iochem-bd) image. It is based on the [*postgres:10.10*](https://hub.docker.com/_/postgres) image to speed up requirement fulfilment.

## Dockerfile instructions detail

It starts by creating the iochembd system user and installing the dependencies: 
  - Java JDK 8
  - gcc 
  - jq, wget, curl, sudo

Then it copies the startup scripts from local */resources* folder to the image; they are used in the first and subsequent runs of the container.

After that, it downloads the ioChem-BD binaries from Gitlab and extracts them on the iochembd user home. 

Finally, it sets the new *startup.sh* command that will install the ioChem-BD software on the first run and in the subsequent cases it will start the Apache Tomcat server.

## Building a base image

To build the image just run:

```bash
$    docker build .
```
It will output the generated image hash, with it you can use it to launch the container, tag it or upload it to any docker registry of your choice.

## Building images from different ioChem-BD releases

The Dockerfile contains an argument named *iochembd* that is associated with the ioChem-BD release used inside the image. If you want to test other releases just provide a new argument:

```bash
$   docker build --build-arg iochembd=2.0.1  . 
```

Then the new vanilla image will hold the defined ioChem-BD release, with no data.

An additional *withdata* boolean argument can be used in order to install sample data and user accounts into the generated image. It can be combined with the previously defined *iochembd* argument.

```bash
    # Predefined version with sample data
$   docker build --build-arg withdata=true .   

    # Combining both parameters, version 2.0.1 with sample data
$   docker build --build-arg iochembd=2.0.1 --build-arg withdata=true .
```



